import pandas as pd
import matplotlib.pyplot as plt
import numpy
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn import metrics
from mlxtend.feature_selection import SequentialFeatureSelector as sfs

def dataAnalysis(data):


    # Exploring data
    print(data.describe().to_string())
    print(data['Gender'].value_counts())
    data['LoanAmount'].hist(bins=50)
    print(data['Credit_History'].unique())
    # data.hist()


    # Check for missing values
    objects = data.columns.values
    bins = data.isnull().sum()
    listOfMissingValuesOccurence = []
    for val in bins:
        listOfMissingValuesOccurence.append(val)
    y_pos = numpy.arange(len(objects))
    performance = listOfMissingValuesOccurence
    fig, ax = plt.subplots()
    plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')
    plt.bar(y_pos, performance, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('number of missing values')
    plt.title('missing value')
    plt.show()
def fillMissingValues(data,train,mean):
    # Fill missing values
    if(train==True):
        data['LoanAmount'].fillna(data['LoanAmount'].mean(), inplace=True)
    else:
        data['LoanAmount'].fillna(mean, inplace=True)

    data.Gender.fillna(data.Gender.mode()[0], inplace=True)
    data.Married.fillna(data.Married.mode()[0], inplace=True)
    data.Self_Employed.fillna(data.Self_Employed.mode()[0], inplace=True)
    data.Credit_History.fillna(data.Credit_History.mode()[0], inplace=True)
    data.Dependents.fillna(data.Dependents.mode()[0], inplace=True)
    data.Loan_Amount_Term.fillna(data.Loan_Amount_Term.mode()[0], inplace=True)
    return data['LoanAmount'].mean()

def encodeCategoricalData(data,test):
    if(test==True):
        var_mod = ['Gender', 'Married', 'Dependents', 'Education', 'Self_Employed', 'Property_Area', 'Loan_Status']
    else:
        var_mod = ['Gender', 'Married', 'Dependents', 'Education', 'Self_Employed', 'Property_Area']
    le = LabelEncoder()
    for i in var_mod:
        data[i] = le.fit_transform(data[i])
def makeOutputFile(results,data2):
    myfile = open("outputfile.csv", 'w')
    myfile.write('Loan_ID,Loan_Status\n')
    for i in range(results.shape[0]):
        myfile.write(data2['Loan_ID'][i])
        myfile.write(",")
        if(results[i]==0):
            myfile.write("N")
        else:
            myfile.write("Y")
        myfile.write('\n')
        i=i+1
def LR(data,data2):
    model = LogisticRegression()

    ###Case of 1 var

    """model.fit(data['Credit_History'].values.reshape(-1, 1), data['Loan_Status'])
    coefficients = model.coef_[0]
    print(coefficients)

    results = model.predict(data2['Credit_History'].values.reshape(-1, 1))
    makeOutputFile(results, data2)"""

    predictor=['Credit_History','Gender','Married']
    dataAsArray= numpy.array(data[predictor])
    model.fit(dataAsArray, data['Loan_Status'])

    coefficients = model.coef_[0]
    print(coefficients)

    data2AsArray = numpy.array(data2[predictor])
    results = model.predict(data2AsArray)
    makeOutputFile(results, data2)

def featureSelection(data,model):

    sfs1 = sfs(model,k_features=3,forward=True,floating=False,verbose=2,scoring='accuracy',cv=5)
    X = data.iloc[:, 1:-1]
    y = data.iloc[:, -1]
    sfs1 = sfs1.fit(X, y)
    feat_cols = list(sfs1.k_feature_idx_)
    print(feat_cols)


if __name__ == "__main__":
    # Read the data
    data = pd.read_csv("train.csv", sep=",")
    dataAnalysis(data)
    mean=fillMissingValues(data,True,-1)
    encodeCategoricalData(data,True)
    print(data.describe(include='all').to_string())

    data2 = pd.read_csv("test.csv", sep=",")
    #fillMissingValues(data2)
    unimportantRV=fillMissingValues(data2,False,mean)
    print(data2.describe(include='all').to_string())
    encodeCategoricalData(data2,False)

    model = LogisticRegression()
    featureSelection(data,model)
    LR(data,data2)





